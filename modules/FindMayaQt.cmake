# This is a CMake module that attempts to find the customized version of Qt
# that Maya ships with.
# ``MAYA_VERSION`` should be defined in order for this module to find the correct version;
# else Maya 2016 is assumed.
# 2016 uses Qt 4.8.6 and 2017 uses Qt 5.6.1.

if (MAYA_VERSION EQUAL 2017)
    message(STATUS "Attempting to find Maya Qt 5.6.1 for Maya 2017...")
    set(${PROJECT_NAME}_VERSION_QT 5.6.1 CACHE STRING "")
    set(QT_VERSION_MAJOR 5)
else()
    # For all other Maya versions, assume Qt 4.8.6
    message(STATUS "Attempting to find Maya Qt 4.8.6 for Maya 2016...")
    set(${PROJECT_NAME}_VERSION_QT 4.8.6 CACHE STRING "")
    set(QT_VERSION_MAJOR 4)
endif()

set(SUFFIX_QT_INSTALL "")
set(SUFFIX_QT_INCLUDE "include" "inc")
set(SUFFIX_QT_LIBRARY "libs" "lib")
set(SUFFIX_QT_BINARY "bin")
set(HEADER_INITIAL_QT "QtCore/qglobal.h")

if (WIN32)
    set(DIR_AUTODESK "$ENV{PROGRAMFILES}/Autodesk")
    if(QT_VERSION_MAJOR EQUAL 5)
        set(LIBRARY_INITIAL_QT "Qt5Core.lib")
    else()
        set(LIBRARY_INITIAL_QT "QtCore4.lib")
    endif()
elseif (APPLE)
    # TODO: Find out what the library names are and have them accounted for here
    set(DIR_AUTODESK /Applications/Autodesk)
    set(LIBRARY_INITIAL_QT libQt5Core.5.dylib)
else()
    set(DIR_AUTODESK /usr/autodesk)
    if (QT_VERSION_MAJOR EQUAL 5)
        set(LIBRARY_INITIAL_QT libQt5Core.so.5)
    else()
        set(LIBRARY_INITIAL_QT libQtCore.so.4)
    endif()

    if(${PROJECT_NAME}_VERSION_QT VERSION_LESS 5.0.0)
        set(SUFFIX_QT_INSTALL -x64)
    endif()
endif()

if (NOT DEFINED ${PROJECT_NAME}_DIR_QT)
    set(${PROJECT_NAME}_DIR_QT
        ${DIR_AUTODESK}/Qt${${PROJECT_NAME}_VERSION_QT}${SUFFIX_QT_INSTALL} CACHE STRING "")
endif()

message(STATUS "Attempting to find Qt header ${HEADER_INITIAL_QT} in:\n
                ${${PROJECT_NAME}_DIR_QT}\n
                $ENV{MAYA_DEVKIT_LOCATION} with path suffixes: ${SUFFIX_QT_INCLUDE}")
find_path(DIR_QT_INCLUDE
    NAMES
        ${HEADER_INITIAL_QT}
    PATHS
        ${${PROJECT_NAME}_DIR_QT}
        ${MAYA_DEVKIT_LOCATION}
        $ENV{MAYA_DEVKIT_LOCATION}
    PATH_SUFFIXES
        ${SUFFIX_QT_INCLUDE}
    NO_DEFAULT_PATH)

if (NOT DIR_QT_INCLUDE)
    message(FATAL_ERROR "Could not find Maya Qt headers! Aborting!")
else()
    message(STATUS "Using Qt headers from: ${DIR_QT_INCLUDE}")
endif()

message(STATUS "Attempting to find Qt library ${LIBRARY_INITIAL_QT} in: \n
                ${${PROJECT_NAME}_DIR_QT}\n
                $ENV{MAYA_DEVKIT_LOCATION} with path suffixes: ${SUFFIX_QT_LIBRARY}")
find_path(DIR_QT_LIBRARY
    NAMES
        ${LIBRARY_INITIAL_QT}
    PATHS
        ${${PROJECT_NAME}_DIR_QT}
        ${MAYA_LOCATION}
        $ENV{MAYA_LOCATION}
    PATH_SUFFIXES
        ${SUFFIX_QT_LIBRARY}
    NO_DEFAULT_PATH)

if (NOT DIR_QT_LIBRARY)
    message(FATAL_ERROR "Could not find Maya Qt libraries! Aborting!")
else()
    message(STATUS "Using Qt headers from: ${DIR_QT_LIBRARY}")
endif()


set(LIBRARIES_QT "")
foreach(LIBRARY_QT ${Qt_FIND_COMPONENTS})

    find_library(LIBRARY_QT_${LIBRARY_QT}
        NAMES
            ${LIBRARY_QT}
        PATHS
            ${DIR_QT_LIBRARY}
        NO_DEFAULT_PATH
    )

    if(LIBRARY_QT_${LIBRARY_QT})
        set(LIBRARIES_QT ${LIBRARIES_QT} ${LIBRARY_QT_${LIBRARY_QT}})
    else()
        find_package_handle_standard_args(Qt DEFAULT_MSG LIBRARY_QT_${LIBRARY_QT})
    endif()

endforeach()

set(CMAKE_INCLUDE_CURRENT_DIR true)
set_property(GLOBAL PROPERTY AUTOGEN_TARGETS_FOLDER CMakePredefinedTargets/_AutoGen)

message(STATUS "Attempting to find Qt moc executable in: \n
                ${${PROJECT_NAME}_DIR_QT}\n
                $ENV{MAYA_DEVKIT_LOCATION} with path suffixes: ${SUFFIX_QT_BINARY}")
find_program(QT_MOC_EXECUTABLE
    NAMES
        moc
    PATHS
        ${${PROJECT_NAME}_DIR_QT}
        ${MAYA_LOCATION}
        $ENV{MAYA_LOCATION}
    PATH_SUFFIXES
        ${SUFFIX_QT_BINARY}
    NO_DEFAULT_PATH)
if (NOT QT_MOC_EXECUTABLE)
    message(FATAL_ERROR "Could not find Maya Qt moc! Aborting!")
else()
    message(STATUS "Using Qt moc from: ${QT_MOC_EXECUTABLE}")
endif()

add_executable(Qt${QT_VERSION_MAJOR}::moc IMPORTED)
set_target_properties(Qt${QT_VERSION_MAJOR}::moc PROPERTIES IMPORTED_LOCATION ${QT_MOC_EXECUTABLE})
set(CMAKE_AUTOMOC ON)

message(STATUS "Attempting to find Qt uic executable in: \n
                ${${PROJECT_NAME}_DIR_QT}\n
                $ENV{MAYA_DEVKIT_LOCATION} with path suffixes: ${SUFFIX_QT_BINARY}")
find_program(QT_UIC_EXECUTABLE
    NAMES
        uic
    PATHS
        ${${PROJECT_NAME}_DIR_QT}
        ${MAYA_LOCATION}
        $ENV{MAYA_LOCATION}
    PATH_SUFFIXES
        ${SUFFIX_QT_BINARY}
    NO_DEFAULT_PATH)
if (NOT QT_UIC_EXECUTABLE)
    message(FATAL_ERROR "Could not find Maya Qt uic! Aborting!")
else()
    message(STATUS "Using Qt uic from: ${QT_UIC_EXECUTABLE}")
endif()

add_executable(Qt${QT_VERSION_MAJOR}::uic IMPORTED)
set_target_properties(Qt${QT_VERSION_MAJOR}::uic PROPERTIES IMPORTED_LOCATION ${QT_UIC_EXECUTABLE})
set(CMAKE_AUTOUIC ON)

message(STATUS "Attempting to find Qt rcc executable in: \n
                ${${PROJECT_NAME}_DIR_QT}\n
                $ENV{MAYA_DEVKIT_LOCATION} with path suffixes: ${SUFFIX_QT_BINARY}")
find_program(QT_RCC_EXECUTABLE
    NAMES
        rcc
    PATHS
        ${${PROJECT_NAME}_DIR_QT}
        ${MAYA_LOCATION}
        $ENV{MAYA_LOCATION}
    PATH_SUFFIXES
        ${SUFFIX_QT_BINARY}
    NO_DEFAULT_PATH)
if (NOT QT_RCC_EXECUTABLE)
    message(FATAL_ERROR "Could not find Maya Qt rcc! Aborting!")
else()
    message(STATUS "Using Qt rcc from: ${QT_RCC_EXECUTABLE}")
endif()

add_executable(Qt${QT_VERSION_MAJOR}::rcc IMPORTED)
set_target_properties(Qt${QT_VERSION_MAJOR}::rcc PROPERTIES IMPORTED_LOCATION ${QT_RCC_EXECUTABLE})
set(CMAKE_AUTORCC ON)

find_package_handle_standard_args(Qt DEFAULT_MSG
    DIR_AUTODESK
    DIR_QT_INCLUDE
    DIR_QT_LIBRARY
    QT_MOC_EXECUTABLE
    QT_UIC_EXECUTABLE
    QT_RCC_EXECUTABLE
    QT_VERSION_MAJOR)
