#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include <QtCore/QList>
#include <maya/MArgDatabase.h>
#include <maya/MGlobal.h>
#include <maya/MStatus.h>
#include <maya/MSyntax.h>
#include "../include/launch_plugin_command.h"

// NOTE: (Siew Yi Liang) Need to delay Maya MQtUtil imports until all other Qt
// imports have been done in order to prevent compiler errors
#include <maya/MQtUtil.h>


const char *flagHelpLongName = "-help";
const char *flagHelpShortName = "-h";

const char *helpText = "This command will launch a test UI that uses Qt.\n"
	"Usage:\n	launchTestQtPlugin [options]\n"
	"Options:\n"
	"-h / -help	Prints this message.\n\n";


void *LaunchPluginCommand::creator()
{
	return new LaunchPluginCommand();
}


MSyntax LaunchPluginCommand::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag(flagHelpShortName, flagHelpLongName);
	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}


MStatus LaunchPluginCommand::doIt(const MArgList &args)
{
	MStatus result;
	this->setCommandString(LaunchPluginCommand::kCOMMAND_NAME);
	MArgDatabase argDb(syntax(), args, &result);
	CHECK_MSTATUS_AND_RETURN_IT(result);

	if (argDb.isFlagSet(flagHelpShortName)) {
		this->displayInfo(helpText);
		return MStatus::kSuccess;

	} else {
		return this->redoIt();
	}
}


MStatus LaunchPluginCommand::redoIt()
{
	MGlobal::displayInfo("Launching the UI...");
	MStatus result;

	const char *toolObjectName = "testToolWidget";
	QWidget *mayaMainWindow = MQtUtil::mainWindow();
	if (mayaMainWindow == NULL) {
		MGlobal::displayError("Could not find the Maya main window!");
		return MStatus::kFailure;
	}
	QList<QWidget *> existingWidgets = mayaMainWindow->findChildren<QWidget *>(toolObjectName);
	if (existingWidgets.isEmpty()) {
		toolWidget = new ToolWindow(mayaMainWindow, Qt::Tool);
		toolWidget->setObjectName(toolObjectName);
		toolWidget->setWindowTitle("Test Widget tool");
	} else {
		toolWidget = (ToolWindow *)existingWidgets[0]; // NOTE: (Siew Yi Liang) We assume the tool is the first widget found
	}
	toolWidget->show();
	toolWidget->raise();

	return result;
}


bool LaunchPluginCommand::isUndoable() const
{
	return false;
}


MStatus LaunchPluginCommand::undoIt()
{
	return MStatus::kSuccess;
}
