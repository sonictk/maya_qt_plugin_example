#include <cstdio>
#include <cstring>
#include <iostream>
#include <maya/MGlobal.h>
#include <QtGui/QMovie>
#include <QtGui/QLabel>
#include "../include/tool_window.h"


ToolWindow::ToolWindow(QWidget *parent, Qt::WindowFlags f)
	: QWidget(parent, f),
	  counter(0)
{
	testComboBox.addItem("Test item 1", "testItem1");
	testComboBox.addItem("Test item 2", "testItem2");
	testComboBox.addItem("Test item 3", "testItem3");
	testButton.setText("Initial button text");

	QMovie *animation = new QMovie(":spr_skeleton_walk_anim.gif");
	QLabel *animationLabel = new QLabel("test label");

	animationLabel->setMovie(animation);
	animation->start();

	testLayout.addWidget(&testComboBox);
	testLayout.addWidget(&testButton);

	testLayout.addWidget(animationLabel);

	testLayout.addStretch();
	this->setLayout(&testLayout);
	bool result = connect(&testButton,
						  SIGNAL(clicked()),
						  this,
						  SLOT(testSlot()));

	if (!result) {
		MGlobal::displayError("The connection could not be made!");
	}
}

ToolWindow::~ToolWindow() {}


void ToolWindow::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::RightButton) {
		testButton.setText("Right button event");
		emit testSignal("signal emitted");
		update();
	} else {
		testButton.setText("Some other event");
		QWidget::mousePressEvent(event);
	}
}


void ToolWindow::testSlot()
{
	const char *signalStr = "signal emitted";
	char counterCStr[std::strlen(signalStr) + 1 + sizeof(unsigned int)];
	std::sprintf(counterCStr, "%s %d", signalStr, counter);
	testComboBox.addItem(counterCStr, "slotItem");
	counter++;
}
