#include <maya/MFnPlugin.h>
#include <maya/MStatus.h>
#include "../include/launch_plugin_command.h"

// Define plugin constants
const char *kAUTHOR = "Siew Yi Liang";
const char *kVERSION = "1.0.0";
const char *kREQUIRED_API_VERSION = "Any";

const MString LaunchPluginCommand::kCOMMAND_NAME = "testQtPlugin";


MStatus initializePlugin(MObject obj)
{
	MStatus status;
	MFnPlugin plugin(obj, kAUTHOR, kVERSION, kREQUIRED_API_VERSION);

	status = plugin.registerCommand(LaunchPluginCommand::kCOMMAND_NAME,
									LaunchPluginCommand::creator,
									LaunchPluginCommand::newSyntax);

	CHECK_MSTATUS_AND_RETURN_IT(status);
	return status;
}


MStatus uninitializePlugin(MObject obj)
{
	MStatus status;
	MFnPlugin plugin(obj);
	status = plugin.deregisterCommand(LaunchPluginCommand::kCOMMAND_NAME);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	return status;
}
