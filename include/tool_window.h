#ifndef TOOL_WINDOW_H
#define TOOL_WINDOW_H
#pragma once

#include <QtCore/Qt>
#include <QtGui/QComboBox>
#include <QtGui/QMouseEvent>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>


class ToolWindow : public QWidget
{
	Q_OBJECT;

public:
	ToolWindow(QWidget *parent=0, Qt::WindowFlags f=0);
	virtual ~ToolWindow();

	QPushButton testButton;
	QComboBox testComboBox;
	QVBoxLayout testLayout;

	unsigned int counter;

public slots:
	void testSlot();

signals:
	void testSignal(const QString &msg);

protected:
	void mousePressEvent(QMouseEvent *event);
};

#endif
