#ifndef LAUNCH_PLUGIN_COMMAND_H
#define LAUNCH_PLUGIN_COMMAND_H
#pragma once

#include <QtGui/QWidget>
#include <QtCore/Qt>
#include "tool_window.h"
#include <maya/MPxCommand.h>
#include <maya/MStatus.h>
#include <maya/MSyntax.h>
#include <maya/MArgList.h>
#include <maya/MString.h>


class LaunchPluginCommand : public MPxCommand
{
public:
	static void *creator();

	/**
	 * This method parses the arguments that were given to the command and stores
	 * it in local class data. It finally calls ``redoIt`` to implement the actual
	 * command functionality.
	 *
	 * @param args	The arguments that were passed to the command.
	 * @return		The status code.
	 */
	MStatus doIt(const MArgList &args);

	/**
	 * This method implements the actual functionality of the command. It is also
	 * called when the user elects to perform an interactive redo of the command.
	 *
	 * @return 	The status code.
	 */
	MStatus redoIt();

	/**
	 * This method is called when the user performs an undo of the command. It
	 * restores the scene to its earlier state before the command was run.
	 *
	 * @return 	The status code.
	 */
	MStatus undoIt();

	/**
	 * This method is used to specify whether or not the command is undoable.
	 *
	 * @return 	``true`` if the command is undo-able, ``false`` otherwise.
	 * 			Should only return ``true`` when the command is executed in
	 * 			non-query mode.
	 */
	bool isUndoable() const;

	/**
	 * This static method returns the syntax object for this command.
	 *
	 * @return The syntax object set up for this command.
	 */
	static MSyntax newSyntax();

	/// The name of the command that is meant to be run.
	static const MString kCOMMAND_NAME;
	ToolWindow *toolWidget;
};


#endif
